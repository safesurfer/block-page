# Block Page

Reference, de-branded implementation of a block page for a Safe Surfer Core deployment. To see an example branding, look at [block.safesurfer.io](https://gitlab.com/safesurfer/block.safesurfer.io). This is a plain HTML/CSS/JS app.

## Branding Guide
Just modify the contents of `index.html`. To change styles, edit `assets/styles.css` or apply [bulma](https://bulma.io/documentation/helpers/) classes.

The following placeholders are used:
- `BRAND_NAME` for the brand name.
- `DASHBOARD_LINK` for a link to the dashboard, which by default is accessed by hitting the icons at the top if you add any.
- `REVIEW_REQUEST_URL` for a link where users can request a change to the website classification.

## Building
Just tag your forked gitlab repo, which will create an image in your repo's container registry. You can then use the container image URL in the values file of your Safe Surfer Core deployment.
