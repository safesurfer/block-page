package main

import (
	"embed"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"strings"
)

//go:embed index.html robots.txt favicon.ico assets/*
var assets embed.FS

func main() {
	fileServer := http.FileServer(http.FS(assets))
	tmpl, err := template.ParseFS(assets, "index.html")
	if err != nil {
		panic(err)
	}
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		// static files
		if strings.Contains(r.URL.Path, ".") {
			fileServer.ServeHTTP(w, r)
			return
		}
		w.WriteHeader(200)
		err = tmpl.Execute(w, map[string]string{
			"CategorizerURL": os.Getenv("APP_CATEGORIZER_URL"),
		})
		if err != nil {
			log.Printf("error executing template: %v\n", err)
		}
	})
	go func() {
		err := http.ListenAndServe("0.0.0.0:8080", nil)
		if err != nil {
			log.Fatal(err)
		}
	}()
	err = http.ListenAndServeTLS(
		fmt.Sprintf("0.0.0.0%v", os.Getenv("APP_HTTPS_PORT")),
		os.Getenv("APP_HTTPS_CRT_PATH"),
		os.Getenv("APP_HTTPS_KEY_PATH"),
		nil,
	)
	if err != nil {
		log.Fatal(err)
	}
}
